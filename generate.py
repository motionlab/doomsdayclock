import time
import requests
import collections
from   datetime    import datetime,    timedelta
from   jinja2      import Environment, FileSystemLoader

def getNextImpact():
    begin = datetime.today().strftime('%Y-%m-%d')
    end   = (datetime.today() + timedelta(days = 7)).strftime('%Y-%m-%d')
    r     = requests.get('https://api.nasa.gov/neo/rest/v1/feed?start_date={}&end_date={}&api_key=DEMO_KEY'.format(begin, end))

    if r.status_code == 200:
        data       = r.json()
        impactdict = {}
        now        = datetime.today()

        for days in range(7):
            newdate    = now + timedelta(days = days)
            datestring = newdate.strftime('%Y-%m-%d')

            if datestring in data['near_earth_objects']:
              for i in data['near_earth_objects'][datestring]:
                ts   = (i['close_approach_data'][0]['epoch_date_close_approach'] / 1000 - time.time()) / 60
                miss = i['close_approach_data'][0]['miss_distance']['kilometers']

                if ts > 0 and ts < 10000:
                    impactdict[miss] = i

        od = collections.OrderedDict(sorted(impactdict.items()))
        return od[next(iter(od))]

theobject   = getNextImpact()
file_loader = FileSystemLoader('templates')
env         = Environment(loader=file_loader)
template    = env.get_template('index.html')
output      = template.render(content=theobject)

print(theobject)

with open("/out/index.html", "w") as fh:
    fh.write(output)
