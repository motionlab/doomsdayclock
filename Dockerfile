FROM python

COPY requirements.txt requirements.txt
RUN  pip  install -r  requirements.txt

ADD generate.py       /generate.py
ADD templates         /templates
