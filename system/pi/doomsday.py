import time
import serial
import requests
import threading
import collections
#import scrollphathd
from   datetime     import datetime, timedelta

def getNextImpact():
    begin = datetime.today().strftime('%Y-%m-%d')
    end   = (datetime.today() + timedelta(days = 7)).strftime('%Y-%m-%d')
    r     = requests.get('https://api.nasa.gov/neo/rest/v1/feed?start_date={}&end_date={}&api_key=DEMO_KEY'.format(begin, end), timeout=10)

    if r.status_code == 200:
        data       = r.json()
        impactdict = {}
        now        = datetime.today()

        for days in range(7):
            newdate    = now + timedelta(days = days)
            datestring = newdate.strftime('%Y-%m-%d')

            if datestring in data['near_earth_objects']:
              for i in data['near_earth_objects'][datestring]:
                ts   = (i['close_approach_data'][0]['epoch_date_close_approach'] / 1000 - time.time()) / 60
                miss = i['close_approach_data'][0]['miss_distance']['kilometers']

                if ts > 0 and ts < 10000:
                    impactdict[miss] = i

        od = collections.OrderedDict(sorted(impactdict.items()))
        return od[next(iter(od))]

def getText(object):
  name = object['name'].replace("(","'").replace(")","'")
  hazardous = object['is_potentially_hazardous_asteroid']
  date_full = object['close_approach_data'][0]['close_approach_date_full']
  distance  = int(float(object['close_approach_data'][0]['miss_distance']['kilometers']))
  velocity  = int(float(object['close_approach_data'][0]['relative_velocity']['kilometers_per_hour']))
  
  if hazardous:
    return "This is serious, we will die!!!!!!! No joke!!!!!"
  else:
    return f"{name} is harmless and will pass by earth on {date_full} with a distance of {distance} km at a speed of {velocity} km/h"

def getTimer(object):
  return int((object['close_approach_data'][0]['epoch_date_close_approach'] / 1000 - time.time()) / 60)

def autoscroll(interval=0.1):
  threading.Timer(interval, autoscroll, [interval]).start()
  scrollphathd.show()
  scrollphathd.scroll()

def setUpDisplay(text):
  scrollphathd.rotate(degrees=180)
  scrollphathd.write_string(text, brightness=0.5)
  autoscroll()

#time.sleep(100)
#theobject = getNextImpact()
#text      = getText(theobject)
#timer     = getTimer(theobject)
#setUpDisplay(text)
#print(text)

#time = -11

#while timer > -1:
#    timer     = getTimer(theobject)
#    timertext = "{:4d}".format(timer)[::-1]

#    with serial.Serial('/dev/ttyS0', 115200, timeout=1) as ser:
#        ser.write(timertext.encode())
#        ser.flushOutput()
#        ser.close()

#    time.sleep(10)
